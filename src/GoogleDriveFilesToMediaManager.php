<?php

namespace Drupal\gg_drive_to_media;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\example\ExampleInterface;
use Drupal\file\Entity\File;
use Drupal\google_api_client\Service\GoogleApiClientService;
use Drupal\media\Entity\Media;
use Drupal\taxonomy\Entity\Term;
use Google\Service\Drive;
use Google\Service\Drive\DriveFile;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\VarDumper\VarDumper;

/**
 * GoogleDriveFilesToMediaManager service.
 */
class GoogleDriveFilesToMediaManager {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The 'gg_drive_to_media.settings' config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The google_api_client.client service.
   *
   * @var \Drupal\google_api_client\Service\GoogleApiClientService
   */
  protected $googleApiClientService;

  /**
   * @var \Google\Service\Drive
   */
  protected $googleDriveService;

  /**
   * Constructs a GoogleDriveFilesToMediaManager object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request_stack.
   * @param \Drupal\google_api_client\Service\GoogleApiClientService $google_api_client_service
   *   The google_api_client.client service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory, RouteMatchInterface $route_match, RequestStack $request_stack, GoogleApiClientService $google_api_client_service) {
    $this->entityTypeManager = $entity_type_manager;
    $this->config = $config_factory->get('gg_drive_to_media.settings');
    $this->routeMatch = $route_match;
    $this->requestStack = $request_stack;
    $this->googleApiClientService = $google_api_client_service;
    $this->setGoogleDriveService();
  }

  protected function setGoogleDriveService() {
    $google_api_client_id = $this->config->get('google_api_client_id');
    /** @var \Drupal\google_api_client\Entity\GoogleApiClient[] $google_api_clients */
    $google_api_clients = $this->entityTypeManager
      ->getStorage('google_api_client')
      ->loadByProperties([
        'client_id' => $google_api_client_id,
      ]);
    $google_api_client = reset($google_api_clients);

    $this->googleApiClientService->setGoogleApiClient($google_api_client);

    $this->googleDriveService = new Drive($this->googleApiClientService->googleClient);

  }


  public function createTaxonomyTerms(string $dir_id, $vid) {
    $query_param = ['q' => '"' . $dir_id . '" in parents and trashed = false and mimeType = "application/vnd.google-apps.folder"'];
    $folder_list = $this->googleDriveService->files->listFiles($query_param);
    $folders = $folder_list->getFiles();
    foreach ($folders as $folder) {
      $term_data = [
        'name' => $folder->getName(),
        'vid' => $vid,
        'field_google_folder_id' => $folder->getId(),
      ];
      if ($term_parent = $this->searchTermByFolderID($dir_id)) {
        $term_data['parent'][] = $term_parent->id();
      }
      $term = Term::create($term_data);
      $term->save();
      $this->createTaxonomyTerms($folder->getId(), $vid);
    }
  }

  public function getTargetTaxonomyTerm($bundle, $media_field) {
    /** @var \Drupal\Core\Field\FieldDefinitionInterface[] $media_fields */
    $media_fields = \Drupal::service('entity_field.manager')->getFieldDefinitions('media', $bundle);
    if (empty($media_fields[$media_field])) {
      return FALSE;
    }
    $field_settings = $media_fields[$media_field]->getSettings();
    if (empty($field_settings['handler_settings']['target_bundles'])) {
      return FALSE;
    }
    $target_bundles = array_keys($field_settings['handler_settings']['target_bundles']);
    return reset($target_bundles);
  }

  /**
   * Check if taxonomy term was created.
   *
   * @param $bundle
   *   Media bundle that need to be migrated.
   * @param $media_field
   *   Media field that's type is entity reference to a vocabulary.
   *
   * @return bool
   *   True If term is exist in the given vocabulary, otherwise fasle.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function checkTargetTaxonomyTerm($bundle, $media_field): bool {
    $vocabulary = $this->getTargetTaxonomyTerm($bundle, $media_field);
    $entity_type_manager = \Drupal::entityTypeManager();
    $taxonomy_terms = $entity_type_manager->getStorage('taxonomy_term')->loadByProperties(['vid' => $vocabulary]);
    return !empty($taxonomy_terms);
  }

  public function searchTermByFolderID($dir_id) {
    $term_storage = $this->entityTypeManager->getStorage('taxonomy_term');
    /** @var \Drupal\taxonomy\Entity\Term[] $terms */
    $terms = $term_storage->loadByProperties([
      'field_google_folder_id' => $dir_id,
    ]);
    $term = reset($terms);
    return $term ?? FALSE;
  }

  protected function getFolderDestination(Term $term) {
    /** @var \Drupal\taxonomy\TermStorage $term_storage */
    $term_storage = $this->entityTypeManager->getStorage('taxonomy_term');
    $parent = $term_storage->loadAllParents($term->id());

    /** @var \Drupal\Core\File\FileSystemInterface $file_system */
    $file_system = \Drupal::service('file_system');
    $folder_to_save_data = 'public://google_drive_folders';
    $file_system->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);


//    $all_folder = $this->getListSubFolder();
//    if (empty($all_folder[$dir_id])) {
//      return 'public://google_drive_folders';
//    }
//    $dir_name = str_replace(" ", "_", $all_folder[$dir_id]['name']);
//    $path[] = $dir_name;
//    $flag = TRUE;
//    while ($flag == TRUE) {
//      $sub_folder = $all_folder[$dir_id];
//      $sub_folder_parent = $sub_folder['parent'];
//      if (!empty($all_folder[$sub_folder_parent])) {
//        $sub_dir_name = str_replace(" ", "_", $all_folder[$sub_folder_parent]['name']);
//        $path[] = $sub_dir_name;
//        $dir_id = $sub_folder_parent;
//      }
//      else {
//        $flag = FALSE;
//      }
//    }
//    $path = array_reverse($path);
//    $final_path = implode('/', $path);
//    return 'public://google_drive_folders/' . $final_path;
  }

  public function checkMediaExist(DriveFile $file) {
    $drupal_file_query = $this->entityTypeManager->getStorage('file')->getQuery();
    $drupal_file_query->condition('filename', $file->getName());
    $drupal_file_query_result = $drupal_file_query->execute();
    if (empty($drupal_file_query_result)) {
      return FALSE;
    }
    $media_query = $this->entityTypeManager->getStorage('media')->getQuery();
    $media_query->condition('name', $file->getName());
    $media_query->condition('bundle', 'google_drive_files');
    $media_query->condition('field_media_document.%delta.target_id', $drupal_file_query_result, 'IN');
    $media_query_result = $media_query->execute();
    return !empty($media_query_result);
  }

  /**
   * Create Media and File entity that were listed in the Google Drive.
   *
   * @param $dir_id
   *   Google Drive folder ID
   * @param $extensions
   *   Allow file extensions that need to be migrated.
   * @param $media_bundle
   *   Target Media bundle when create media entity.
   * @param $term_field
   *   The field of media bundle to set the reference to taxonomy term.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createMediaFromFolderId(Term $term, $extensions, $media_bundle, $term_field) {
    $this->getFolderDestination($term);
    $dir_id = $term->get('field_google_folder_id')->getString();
    // Set pattern base on extension list to filter item need to be migrated.
    $allowed_extension = str_replace(" ", "|", $extensions);
    $pattern = '/^.*\.(' . $allowed_extension . ')$/i';

    $query_param = ['q' => '"' . $dir_id . '" in parents and trashed = false'];
    $list_files = $this->googleDriveService->files->listFiles($query_param);
    $files = $list_files->getFiles();

    /** @var \Drupal\Core\File\FileSystemInterface $file_system */
    $file_system = \Drupal::service('file_system');
    $folder_to_save_data = 'public://google_drive_folders';
    $file_system->prepareDirectory($folder_to_save_data, FileSystemInterface::CREATE_DIRECTORY);

    foreach ($files as $file) {
      // Check file extension and check media existing,
      // Before process creating media and File entity.
      if (preg_match($pattern, $file->getName()) && !$this->checkMediaExist($file)) {
        /** @var \GuzzleHttp\Psr7\Response $file_response */
        $file_response = $this->googleDriveService->files->get($file->getId(), ['alt' => 'media']);
        $file_content = $file_response->getBody();
        $file_destination = $folder_to_save_data . '/' . $file->getName();
        $drupal_file = file_save_data($file_content,  $file_destination);
        $drupal_file->setPermanent();
        $drupal_file->save();
        $media_data = [
          'bundle' => $media_bundle,
          'name' => $file->getName(),
          // @todo Make field_media_document become a variable.
          'field_media_document' => [
            'target_id' => $drupal_file->id(),
          ],
        ];
        if (!empty($term)) {
          $media_data[$term_field] = [
            'target_id' => $term->id(),
          ];
        }
        $media = Media::create($media_data);
        $media->setPublished();
        $media->save();
      }
    }

    // Mark the migration as done after migrate all file inside this folder.
    $term->set('field_migration_done', 1);
    $term->save();
  }

}

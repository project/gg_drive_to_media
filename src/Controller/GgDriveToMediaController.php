<?php

namespace Drupal\gg_drive_to_media\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\google_api_client\Service\GoogleApiClientService;
use Exception;
use Google\Service\Drive;
use Google\Service\Drive\DriveFile;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\VarDumper\VarDumper;

/**
 * Returns responses for GG Drive To Media routes.
 */
class GgDriveToMediaController extends ControllerBase {

  /**
   * @var \Drupal\google_api_client\Service\GoogleApiClientService
   */
  protected $googleApiClientService;

  /**
   * @var \Google\Service\Drive
   */
  protected $googleDriveService;

  /**
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   *
   * @var \Drupal\gg_drive_to_media\GoogleDriveFilesToMediaManager
   */
  protected $ggDriveToMedia;

  /**
   * @param \Drupal\google_api_client\Service\GoogleApiClientService $googleApiClientService
   */
  public function __construct(GoogleApiClientService $googleApiClientService) {
    $this->googleApiClientService = $googleApiClientService;
    try {
      $config = $this->config('gg_drive_to_media.settings');
      $google_api_client_id = $config->get('google_api_client_id');
      /** @var \Drupal\google_api_client\Entity\GoogleApiClient[] $google_api_clients */
      $google_api_clients = $this->entityTypeManager()
        ->getStorage('google_api_client')
        ->loadByProperties([
          'client_id' => $google_api_client_id,
        ]);
      $google_api_client = reset($google_api_clients);

      $this->googleApiClientService->setGoogleApiClient($google_api_client);

      $this->googleDriveService = new Drive($this->googleApiClientService->googleClient);
    }
    catch (Exception $Exception) {
    }
    $this->routeMatch = \Drupal::routeMatch();
    $this->currentRequest = \Drupal::request();
    $this->ggDriveToMedia = \Drupal::service('gg_drive_to_media.manager');


  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('google_api_client.client')
    );
  }

  /**
   * Builds the response.
   */
  public function build(string $dir_id = 'root') {

    $params = [
      'supportsAllDrives' => true,
      'supportsTeamDrives' => true,
      'orderBy' => 'folder',
      'q' => '"' . $dir_id . '" in parents and trashed = false',
      'fields' => ['nextPageToken, files(mimeType, id, name, webViewLink, iconLink, parents, shortcutDetails)'],
    ];
    try {
      $files = $this->googleDriveService->files->listFiles($params);
      $file_list = $files->getFiles();
    }
    catch (\Google\Service\Exception $exception) {

      return [
        '#type' => 'markup',
        '#markup' => $exception->getMessage(),
      ];
    }

    $header = [
      'file_name' => $this->t('File Name'),
      'migrate_action' => $this->t("Migrate Action"),
    ];
    $rows = [];
    foreach ($file_list as $file) {
      $row = [];
      $file_mine_type = $file->getMimeType();
      $file_name = $file->getName();

      if ($file_mine_type == 'application/vnd.google-apps.folder' || $file_mine_type == 'application/vnd.google-apps.shortcut') {
        if ($file_mine_type == 'application/vnd.google-apps.shortcut') {
          if ($file->getShortcutDetails()->getTargetMimeType() !== 'application/vnd.google-apps.folder') {
            $row[] = $file->getName();
            continue;
          }
          $file_id = $file->getShortcutDetails()->getTargetId();
        }
        else {
          $file_id = $file->getId();
        }
        $row[] = $this->getChildFolderLink($file_name, $file_id)->toString();
        $link = Link::createFromRoute(
          'Migrate files inside this folder',
          'gg_drive_to_media.migrate',
          ['dir_id' => $file_id]
        );
        $row[] = $link;
      }
      else {

        $row[] = $file->getName();
      }
      $rows[] = $row;
    }

    $build['content'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];

    return $build;
  }

  protected function getBackLink($file_id) {
    $parents = $this->googleDriveService->files->get($file_id, ['fields' => ['id', 'name', 'parents']])->getParents();
    if (!empty($parents)) {
      $parent = reset($parents);
      $current_route_params = $this->routeMatch->getParameters();
      $current_route_params['dir_id'] = $parent;
      return Link::createFromRoute("Back", $this->routeMatch->getRouteName(), $current_route_params);
    }
    return FALSE;
  }

  protected function getChildFolderLink(string $title, string $id) {
    $current_route_params = $this->routeMatch->getParameters();
    $current_route_params->set('dir_id', $id);
    return Link::createFromRoute($title, $this->routeMatch->getRouteName(), $current_route_params->all());
  }

//  protected function getCurrentPage() {
//    $dir_id = $this->routeMatch->getParameter('dir_id');
//    $dir_id = $dir_id ?? 'root';
//
//  }

  public function titleCallBack(string $dir_id) {
    $current_folder_name = $this->googleDriveService->files->get($dir_id)->getName();

  }


}

<?php

namespace Drupal\gg_drive_to_media\Form;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\gg_drive_to_media\GoogleDriveFilesToMediaManager;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\VarDumper\VarDumper;

/**
 * Provides a GG Drive To Media form.
 */
class MigrateDriveFileToMediaForm extends FormBase {

  public const migrationId = 'migrate_media_from_gg_drive_folder';

  /**
   * @var \Drupal\gg_drive_to_media\GoogleDriveFilesToMediaManager
   */
  protected $ggDriveToMedia;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * Construct Migrate Object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\gg_drive_to_media\GoogleDriveFilesToMediaManager $ggDriveToMedia
   *   The gg_drive_to_media.manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity_field.manager service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityTypeBundleInfo
   *   The entity_type.bundle.info service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager,
    GoogleDriveFilesToMediaManager $ggDriveToMedia,
    EntityFieldManagerInterface $entityFieldManager,
    EntityTypeBundleInfoInterface $entityTypeBundleInfo) {
    $this->ggDriveToMedia = $ggDriveToMedia;
    $this->entityTypeManager = $entityTypeManager;
    $this->entityFieldManager = $entityFieldManager;
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('gg_drive_to_media.manager'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.bundle.info')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gg_drive_to_media_migrate_drive_file_to_media';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['length'] = [
      '#type' => 'number',
      '#title' => 'Process max this many items',
      '#required' => TRUE,
      '#default_value' => 1000,
    ];
    $form['specific_id'] = [
      '#type' => 'textfield',
      '#title' => 'Specific folder ID to process',
      '#required' => FALSE,
    ];

    $form['media_bundle'] = [
      '#type' => 'select',
      '#required' => TRUE,
      '#title' => $this->t('Select media bundle to migrate:'),
      '#options' => $this->getMediaBundle(),
      '#default_value' => 'none',
      '#ajax' => [
        'wrapper' => 'media-field-list',
        'callback' => '::promptCallbackMediaField',
      ],
    ];

    $media_bundle = $form_state->getValue('media_bundle');

    $form['media_field_wrapper'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'media-field-list'],
    ];
    $form['media_field_wrapper']['media_term_field'] = [
      '#type' => 'select',
      '#required' => TRUE,
      '#title' => $this->t('Select field that map the taxonomy term as folder tree:'),
      '#description' => $this->t('Each media will have its own folder parent. When migrating the media, this field would be the reference to taxonomy term.'),
      '#options' => static::getFieldEntityReference($media_bundle),
    ];

    $form['file_extension'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Allowed File Extensions'),
      '#description' => $this->t('The file extensions that will be effected in the migration. Use space for separating like: "mp4 jpeg gif".'),
      '#required' => TRUE,
    ];

    $form['actions_wrapper'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'actions-field-wrapper'],
    ];
    $form['actions_wrapper']['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions_wrapper']['actions']['create_term'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create Term base on folder structure'),
      '#submit' => [[$this, 'createTerm']],
    ];

//    $media_field = $form_state->getValue(['media_field_wrapper','media_field']);
//
//    if ($this->checkTargetTaxonomyTerm($media_bundle, $media_field)) {
//
//    }

    $form['actions_wrapper']['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create Media'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $file_extension = $form_state->getValue('file_extension');
    $dir_id = $this->getRequest()->query->get('dir_id') ?? 'root';

    $batch = [
      'title' => t('Updating items...'),
      'operations' => [],
      'init_message' => t('Commencing'),
      'progress_message' => t('Processed @current out of @total.'),
      'error_message' => t('An error occurred during processing'),
      'finished' => [$this, 'updateFinishedCallback'],
    ];
    $folder_id = $form_state->getValue('specific_id');
    $length = $form_state->getValue('length');
    $bundle = $form_state->getValue('media_bundle');
    $term_field = $form_state->getValue('media_term_field');
    $vid = $this->ggDriveToMedia->getTargetTaxonomyTerm($bundle, $term_field);
    $term_query = $this->entityTypeManager->getStorage('taxonomy_term')->getQuery();
    $term_query->condition('vid', $vid);
    if (!empty($folder_id)) {
      $term_query->condition('field_google_folder_id', $folder_id);
    }
    else {
      $orCondition =$term_query->orConditionGroup()
        ->condition('field_migration_done', 0)
        ->notExists('field_migration_done');
      $term_query->condition($orCondition);
      $term_query
        ->range(0, $length);
    }
    $terms = $term_query->execute();

    foreach ($terms as $term_id) {
      /** @var \Drupal\taxonomy\Entity\Term $term */
      $term = $this->entityTypeManager->getStorage('taxonomy_term')->load($term_id);
      $term_folder_id = $term->get('field_google_folder_id')->getString();
      $batch['operations'][] = [[$this, 'createMedia'], [$term, $file_extension, $bundle, $term_field]];
    }

    batch_set($batch);
  }

  public static function createMedia(Term $term, $file_extension, $bundle, $term_field, &$context) {

    \Drupal::logger(self::migrationId)->info('Start processing taxonomy term @tid: @name', [
      '@name' => $term->label(),
      '@tid' => $term->id(),
    ]);

    $context['results']['processed'] = $context['results']['processed'] ?? 0;
    $context['results']['processed']++;
    $context['message'] = "Processing entity id " . $term->id();

    /** @var \Drupal\gg_drive_to_media\GoogleDriveFilesToMediaManager $ggDriveToMedia */
    $ggDriveToMedia = \Drupal::service('gg_drive_to_media.manager');
    $ggDriveToMedia->createMediaFromFolderId($term, $file_extension, $bundle, $term_field);

  }

  public function createTerm(array &$form, FormStateInterface $form_state) {

    $bundle = $form_state->getValue('media_bundle');
    $term_field = $form_state->getValue('media_term_field');
    $dir_id = $this->getRequest()->query->get('dir_id') ?? 'root';
    $vid = $this->ggDriveToMedia->getTargetTaxonomyTerm($bundle, $term_field);
    $this->ggDriveToMedia->createTaxonomyTerms($dir_id, $vid);
  }

  /**
   * List Media bundles are available for migration.
   *
   * @return array
   *   Option list of media bundle as ID => Name.
   */
  public function getMediaBundle() {
    $list_media_bundle = \Drupal::service('entity_type.bundle.info')->getBundleInfo('media');
    $result = [];
    foreach ($list_media_bundle as $bundle_id => $name) {
      $result[$bundle_id] = $name['label'];
    }
    return $result;
  }

  /**
   * Get list of entity reference to taxonomy term of media bundle.
   *
   * @param $bundle
   *   Media bundle
   *
   * @return array
   *   List of available field to set the taxonomy term reference.
   */
  public function getFieldEntityReference($bundle) {
    /** @var \Drupal\Core\Field\FieldDefinitionInterface[] $media_fields */
    $media_fields = \Drupal::service('entity_field.manager')->getFieldDefinitions('media', $bundle);
    $result = [];
    foreach ($media_fields as $field_id => $field) {
      if ($field->getType() !== 'entity_reference') {
        continue;
      }
      $field_settings = $field->getSettings();
      if (!empty($field_settings['target_type']) && $field_settings['target_type'] === 'taxonomy_term') {
        $result[$field_id] = $field->getName();
      }
    }
    return $result;
  }

  /**
   * Ajax call back.
   */
  public function promptCallbackMediaField(array $form, FormStateInterface $form_state) {
    return $form['media_field_wrapper'];
  }

  /**
   * Batch processing finished callback.
   */
  public static function updateFinishedCallback($success, $results, $operations) {
    if ($success) {
      $message = $results['processed'] . ' items processed.';
    }
    else {
      $message = 'Finished with an error.';
    }
    \Drupal::logger(self::migrationId)->info($message);
    \Drupal::messenger()->addMessage($message);
  }

}

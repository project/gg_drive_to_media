<?php

namespace Drupal\gg_drive_to_media\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\google_api_client\Entity\GoogleApiClient;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\VarDumper\VarDumper;

/**
 * Configure GG Drive To Media settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a SiteInformationForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gg_drive_to_media_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['gg_drive_to_media.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('gg_drive_to_media.settings');
    if ($google_api_client_id = $config->get('google_api_client_id')) {
      /** @var \Drupal\google_api_client\Entity\GoogleApiClient[] $google_api_clients */
      $google_api_clients = $this->entityTypeManager
        ->getStorage('google_api_client')
        ->loadByProperties([
            'client_id' => $google_api_client_id,
        ]);
      $google_api_client = reset($google_api_clients);
      if ($google_api_client instanceof GoogleApiClient && !$google_api_client->getAuthenticated()) {
        $authenticate_link = Link::createFromRoute($this->t('Authenticate now'),
          'google_api_client.callback',
          [
            'id' => $google_api_client->getId(),
            'destination' => '/admin/config/services/gg_media_to_drive',
          ],
          ['attributes' => ['target' => '_blank']])->toString();
        $this->messenger()->addWarning($this->t('You need to authenticate with google, %link', ['%link' => $authenticate_link]));
      }
    }
    $form['dd_drive_to_media']['google_client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#default_value' => is_object($google_api_client) ? $google_api_client->getClientId() : '',
      '#required' => TRUE,
      '#size' => 100,
      '#maxlength' => 150,
      '#description' => $this->t('The site wide google client id.'),
    ];
    $form['dd_drive_to_media']['google_client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client secret'),
      '#default_value' => is_object($google_api_client) ? $google_api_client->getClientSecret() : '',
      '#required' => TRUE,
      '#description' => $this->t('The site wide google client secret.'),
    ];
    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if ($values['google_api_client_id']) {
      /** @var \Drupal\google_api_client\Entity\GoogleApiClient $google_api_client */
      $google_api_client = $this->entityTypeManager
        ->getStorage('google_api_client')
        ->loadByProperties([
          'client_id' => $values['google_api_client_id'],
        ]);
      $google_api_client->setClientId($values['droogle_client_id']);
      $google_api_client->setClientSecret($values['droogle_client_secret']);
      $google_api_client->setAccessType($values['droogle_drive_connect_mode']);
    }
    else {
      $account = [
        'name' => 'GG Drive To Media - Google Api Client',
        'client_id' => $values['google_client_id'],
        'client_secret' => $values['google_client_secret'],
        'access_token' => '',
        'services' => ['drive'],
        'is_authenticated' => FALSE,
        'scopes' => ['DRIVE'],
        'access_type' => 0,
      ];
      $google_api_client = $this->entityTypeManager->getStorage('google_api_client')->create($account);
    }
    $google_api_client->save();
    $this->config('gg_drive_to_media.settings')
      ->set('google_api_client_id', $google_api_client->getClientId())
      ->save();
    parent::submitForm($form, $form_state);
  }

}

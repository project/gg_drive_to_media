1. Enter the google client ID and Client secret at: /admin/config/services/gg-drive-to-media-settings
2. Authenticate the existing Google Client API entity at: /admin/config/services/google_api_client
3. Go to /gg-drive-to-media/list-drive to select which folder that want to migrate
4. Select "Migrate files inside this folder" on the right column to go to migration page.
5. Fill in the file extension that allowed to be migrated
6. Check /admin/structure/taxonomy/manage/google_drive_folder/overview for the list folder as taxonomy term
7. Check /admin/content/media?name=&type=google_drive_files&status=All&langcode=All for the view list of migrated files.